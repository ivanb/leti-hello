# Hello World

First, make `hello` executable:
```
chmod +x hello
```

Second, run the program:
```
./hello
```
